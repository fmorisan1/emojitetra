package sondow.tetra.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sondow.tetra.shape.ShapeType;

import static org.junit.Assert.assertEquals;

public class EmojiSetTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testShowcase() {
        assertEquals("🐷🐯🐵🐱🐻🐨🐼", EmojiSet.ANIMAL.showcase());
        assertEquals("🐙🐡🐠🐬🐟🐳🐌", EmojiSet.AQUATIC.showcase());
        assertEquals("📕📙📒📗📘📔📚", EmojiSet.BOOK.showcase());
        assertEquals("👚👘👔👗🎽👕👖", EmojiSet.CLOTHING.showcase());
        assertEquals("🍎🍑🍋🍏🍇🍉🍓", EmojiSet.FRUIT.showcase());
        assertEquals("👺🎃😱🍭🔮😈🍫", EmojiSet.HALLOWEEN.showcase());
        assertEquals("💘💖💛💚💙💜💗", EmojiSet.HEART.showcase());
        assertEquals("🌸🌻🌼🍀🌿🌺🍄", EmojiSet.PLANT.showcase());
    }

    @Test
    public void testBasedOnShowcase() {
        assertEquals(EmojiSet.ANIMAL, EmojiSet.basedOnShowcase("🐷🐯🐵🐱🐻🐨🐼"));
        assertEquals(EmojiSet.AQUATIC, EmojiSet.basedOnShowcase("🐙🐡🐠🐬🐟🐳🐌"));
        assertEquals(EmojiSet.BOOK, EmojiSet.basedOnShowcase("📕📙📒📗📘📔📚"));
        assertEquals(EmojiSet.CLOTHING, EmojiSet.basedOnShowcase("👚👘👔👗🎽👕👖"));
        assertEquals(EmojiSet.FRUIT, EmojiSet.basedOnShowcase("🍎🍑🍋🍏🍇🍉🍓"));
        assertEquals(EmojiSet.HALLOWEEN, EmojiSet.basedOnShowcase("👺🎃😱🍭🔮😈🍫"));
        assertEquals(EmojiSet.HEART, EmojiSet.basedOnShowcase("💘💖💛💚💙💜💗"));
        assertEquals(EmojiSet.PLANT, EmojiSet.basedOnShowcase("🌸🌻🌼🍀🌿🌺🍄"));
    }

    @Test
    public void testPickAny() {
        Random random = new Random(123L);
        List<EmojiSet> actual = EmojiSet.pickAny(4, random);
        List<EmojiSet> expected = Arrays.asList(EmojiSet.HALLOWEEN, EmojiSet.AQUATIC,
                EmojiSet.PLANT, EmojiSet.BOOK);
        assertEquals(expected, actual);
    }

    @Test
    public void testAnimalSymbolForShapeType() {
        assertEquals("🐷", EmojiSet.ANIMAL.symbolFor(ShapeType.ZEE));
        assertEquals("🐯", EmojiSet.ANIMAL.symbolFor(ShapeType.LINE));
        assertEquals("🐵", EmojiSet.ANIMAL.symbolFor(ShapeType.TEE));
        assertEquals("🐱", EmojiSet.ANIMAL.symbolFor(ShapeType.JAY));
        assertEquals("🐻", EmojiSet.ANIMAL.symbolFor(ShapeType.SQUARE));
        assertEquals("🐨", EmojiSet.ANIMAL.symbolFor(ShapeType.ELL));
        assertEquals("🐼", EmojiSet.ANIMAL.symbolFor(ShapeType.ESS));

        assertEquals("🐙", EmojiSet.AQUATIC.symbolFor(ShapeType.ZEE));
        assertEquals("🐡", EmojiSet.AQUATIC.symbolFor(ShapeType.LINE));
        assertEquals("🐠", EmojiSet.AQUATIC.symbolFor(ShapeType.TEE));
        assertEquals("🐬", EmojiSet.AQUATIC.symbolFor(ShapeType.JAY));
        assertEquals("🐟", EmojiSet.AQUATIC.symbolFor(ShapeType.SQUARE));
        assertEquals("🐳", EmojiSet.AQUATIC.symbolFor(ShapeType.ELL));
        assertEquals("🐌", EmojiSet.AQUATIC.symbolFor(ShapeType.ESS));

        assertEquals("📕", EmojiSet.BOOK.symbolFor(ShapeType.ZEE));
        assertEquals("📙", EmojiSet.BOOK.symbolFor(ShapeType.LINE));
        assertEquals("📒", EmojiSet.BOOK.symbolFor(ShapeType.TEE));
        assertEquals("📗", EmojiSet.BOOK.symbolFor(ShapeType.JAY));
        assertEquals("📘", EmojiSet.BOOK.symbolFor(ShapeType.SQUARE));
        assertEquals("📔", EmojiSet.BOOK.symbolFor(ShapeType.ELL));
        assertEquals("📚", EmojiSet.BOOK.symbolFor(ShapeType.ESS));

        assertEquals("👚", EmojiSet.CLOTHING.symbolFor(ShapeType.ZEE));
        assertEquals("👘", EmojiSet.CLOTHING.symbolFor(ShapeType.LINE));
        assertEquals("👔", EmojiSet.CLOTHING.symbolFor(ShapeType.TEE));
        assertEquals("👗", EmojiSet.CLOTHING.symbolFor(ShapeType.JAY));
        assertEquals("🎽", EmojiSet.CLOTHING.symbolFor(ShapeType.SQUARE));
        assertEquals("👕", EmojiSet.CLOTHING.symbolFor(ShapeType.ELL));
        assertEquals("👖", EmojiSet.CLOTHING.symbolFor(ShapeType.ESS));

        assertEquals("🍎", EmojiSet.FRUIT.symbolFor(ShapeType.ZEE));
        assertEquals("🍑", EmojiSet.FRUIT.symbolFor(ShapeType.LINE));
        assertEquals("🍋", EmojiSet.FRUIT.symbolFor(ShapeType.TEE));
        assertEquals("🍏", EmojiSet.FRUIT.symbolFor(ShapeType.JAY));
        assertEquals("🍇", EmojiSet.FRUIT.symbolFor(ShapeType.SQUARE));
        assertEquals("🍉", EmojiSet.FRUIT.symbolFor(ShapeType.ELL));
        assertEquals("🍓", EmojiSet.FRUIT.symbolFor(ShapeType.ESS));

        assertEquals("👺", EmojiSet.HALLOWEEN.symbolFor(ShapeType.ZEE));
        assertEquals("🎃", EmojiSet.HALLOWEEN.symbolFor(ShapeType.LINE));
        assertEquals("😱", EmojiSet.HALLOWEEN.symbolFor(ShapeType.TEE));
        assertEquals("🍭", EmojiSet.HALLOWEEN.symbolFor(ShapeType.JAY));
        assertEquals("🔮", EmojiSet.HALLOWEEN.symbolFor(ShapeType.SQUARE));
        assertEquals("😈", EmojiSet.HALLOWEEN.symbolFor(ShapeType.ELL));
        assertEquals("🍫", EmojiSet.HALLOWEEN.symbolFor(ShapeType.ESS));

        assertEquals("💘", EmojiSet.HEART.symbolFor(ShapeType.ZEE));
        assertEquals("💖", EmojiSet.HEART.symbolFor(ShapeType.LINE));
        assertEquals("💛", EmojiSet.HEART.symbolFor(ShapeType.TEE));
        assertEquals("💚", EmojiSet.HEART.symbolFor(ShapeType.JAY));
        assertEquals("💙", EmojiSet.HEART.symbolFor(ShapeType.SQUARE));
        assertEquals("💜", EmojiSet.HEART.symbolFor(ShapeType.ELL));
        assertEquals("💗", EmojiSet.HEART.symbolFor(ShapeType.ESS));

        assertEquals("🌸", EmojiSet.PLANT.symbolFor(ShapeType.ZEE));
        assertEquals("🌻", EmojiSet.PLANT.symbolFor(ShapeType.LINE));
        assertEquals("🌼", EmojiSet.PLANT.symbolFor(ShapeType.TEE));
        assertEquals("🍀", EmojiSet.PLANT.symbolFor(ShapeType.JAY));
        assertEquals("🌿", EmojiSet.PLANT.symbolFor(ShapeType.SQUARE));
        assertEquals("🌺", EmojiSet.PLANT.symbolFor(ShapeType.ELL));
        assertEquals("🍄", EmojiSet.PLANT.symbolFor(ShapeType.ESS));
    }

    @Test
    public void testPickOne() {
        List<EmojiSet> sets = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            Random random = new Random(i);
            EmojiSet result = EmojiSet.pickOne(random);
            sets.add(result);

        }
        assert Collections.frequency(sets, EmojiSet.PLANT) > 330;
        assert Collections.frequency(sets, EmojiSet.CLOTHING) > 330;
        assert Collections.frequency(sets, EmojiSet.HALLOWEEN) > 330;
    }
}
