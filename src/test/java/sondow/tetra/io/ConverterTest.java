package sondow.tetra.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import sondow.tetra.game.Game;
import sondow.tetra.shape.ShapeType;
import twitter4j.JSONException;

import static org.junit.Assert.assertEquals;

public class ConverterTest {

    @Test
    public void testWithLotsaStuffOnBottomNoPreviousMove() throws IOException, JSONException {
        String contents = "{\"thm\":\"FRUIT\",\"thmLen\":57,\"thr\":51," +
                "\"rows\":\"3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ\"," +
                "\"cur\":{\"t\":\"J\",\"c\":3,\"r\":0,\"d\":\"S\"},\"nx\":\"L\",\"sc\":1500," +
                "\"id\":987654321}";
        Converter converter = new Converter(new Random(4));
        Game game = converter.makeGameFromJson(contents);

        String expected = "" +
                "Next ⠧　Score 1500\n\n" +
                "◽◽🍏◽◽◽◽\n" +
                "◽◽🍏🍏🍏◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🍋◽◽◽\n" +
                "🍉◽🍋🍋🍋🍇🍇\n" +
                "🍉🍉🍓🍏◽🍓🍓\n" +
                "◽🍓🍓🍏🍓🍓🍉\n" +
                "🍓🍓🍇🍇◽🍉🍉\n" +
                "◽🍋🍇🍇🍏🍏🍏\n" +
                "🍋🍋🍋◽🍎🍎🍏\n" +
                "◽🍑🍑🍑🍑🍎🍎";
        assertEquals(51, game.getThreadLength());
        assertEquals(expected, game.toString());
        assertEquals(1500, game.getScore());
        assertEquals(null, game.getPiece().getPreviousMove());
        assertEquals(ShapeType.ELL, game.getNextShape());
    }

    @Test
    public void testCompressAsciiRowsComplexGame() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                ".......",
                ".......",
                ".......",
                "...T...",
                "L.TTTOO",
                "LLSJ.SS",
                ".SSJSSL",
                "SOO.LL",
                ".TOOJJJ",
                "TTT.ZZJ",
                ".IIIIZZ");
        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);

        assertEquals("3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ", result1);
    }

    @Test
    public void testCompressAsciiRowsAllBlank() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                ".......");

        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);
        assertEquals("9*.7-2*.7", result1);
    }

    @Test
    public void testCompressAsciiRowsWideAndTall() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............",
                "..............");

        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);
        assertEquals("9*.9.5-6*.9.5", result1);
    }

    @Test
    public void testCompressAsciiRows8SameEnd() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                ".......",
                ".......",
                ".......",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ");
        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);

        assertEquals("3*.7-8*T3.ZZJ", result1);
    }

    @Test
    public void testCompressAsciiRows9SameEnd() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                ".......",
                ".......",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ");
        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);

        assertEquals("2*.7-9*T3.ZZJ", result1);
    }

    @Test
    public void testCompressAsciiRows10SameEnd() throws IOException, JSONException {
        List<String> rows1 = Arrays.asList(
                ".......",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ",
                "TTT.ZZJ");
        Converter converter = new Converter(null);
        String result1 = converter.compressAsciiRows(rows1);

        assertEquals(".7-9*T3.ZZJ-T3.ZZJ", result1);
    }

    private String readFile(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        @SuppressWarnings("ConstantConditions")
        String filePath = classLoader.getResource(fileName).getFile();
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

    @Test
    public void testExpandEncodedRows() {
        Converter converter = new Converter(null);

        List<String> latinRowsSimple = converter.expandEncodedRows
                (".7-.7-.7-.7-.7-.7-.7-.7-.7-.7-.I4ZZ");
        List<String> expectedSimple = Arrays.asList(".......", ".......", ".......", ".......",
                ".......", ".......", ".......", ".......", ".......", ".......", ".IIIIZZ");
        assertEquals(expectedSimple, latinRowsSimple);

        String complex = ".7-.7-.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ";
        List<String> latinRowsComplex = converter.expandEncodedRows(complex);
        List<String> expectedComplex = Arrays.asList(".......", ".......", ".......", "...T...",
                "L.TTTOO", "LLSJ.SS", ".SSJSSL", "SSOO.LL", ".TOOJJJ", "TTT.ZZJ", ".IIIIZZ");
        assertEquals(expectedComplex, latinRowsComplex);
    }

    @Test
    public void testExpandEncodedRowsTight() {
        Converter converter = new Converter(null);

        List<String> latinRowsSimple = converter.expandEncodedRows
                ("9*.7-.7-.I4ZZ");
        List<String> expectedSimple = Arrays.asList(".......", ".......", ".......", ".......",
                ".......", ".......", ".......", ".......", ".......", ".......", ".IIIIZZ");
        assertEquals(expectedSimple, latinRowsSimple);

        String complex = "3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ";
        List<String> latinRowsComplex = converter.expandEncodedRows(complex);
        List<String> expectedComplex = Arrays.asList(".......", ".......", ".......", "...T...",
                "L.TTTOO", "LLSJ.SS", ".SSJSSL", "SSOO.LL", ".TOOJJJ", "TTT.ZZJ", ".IIIIZZ");
        assertEquals(expectedComplex, latinRowsComplex);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testExpandEncodedRowsDigitFirstThrowsRuntimeException() {
        Converter converter = new Converter(null);
        exception.expect(RuntimeException.class);
        exception.expectMessage("java.text.ParseException: Digit found at start of compressed row" +
                " 7. in game data .7-.7-.7-.7-7.-.7-.7-.7-.7-.7-.I4ZZ");
        converter.expandEncodedRows(".7-.7-.7-.7-7.-.7-.7-.7-.7-.7-.I4ZZ");
    }

    @Test
    public void testCharacterGetNumericValue() {
        assertEquals(0, Character.getNumericValue('0'));
        assertEquals(1, Character.getNumericValue('1'));
        assertEquals(3, Character.getNumericValue('3'));
        assertEquals(9, Character.getNumericValue('9'));
    }

    @Test
    public void testRoundTripWithLotsaStuffOnBottom() throws IOException, JSONException {
        String contents = "{\"thm\":\"FRUIT\",\"thmLen\":57,\"thr\":51," +
                "\"rows\":\"3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ\"," +
                "\"cur\":{\"t\":\"J\",\"c\":3,\"r\":0,\"d\":\"S\"},\"nx\":\"L\",\"sc\":1500," +
                "\"id\":987654321}";
        Converter converter = new Converter(null);
        Game game1 = converter.makeGameFromJson(contents);
        String jsonFromGame1 = converter.makeJsonFromGame(game1);
        Game game2 = converter.makeGameFromJson(jsonFromGame1);
        String jsonFromGame2 = converter.makeJsonFromGame(game2);
        assertEquals(game1, game2);
        assertEquals(jsonFromGame1, jsonFromGame2);
    }

}
