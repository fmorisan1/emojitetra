package sondow.tetra.shape;

import sondow.tetra.game.BadCoordinatesException;
import sondow.tetra.game.Direction;

/**
 * <code>ShapeEss</code> is a configuration of <code>Tile</code> objects resembling the letter
 * "S" in shape (see NORTH and SOUTH configurations below).
 * <p>
 * The configuration of this piece for each direction appears as follows. The rotation symbol in
 * each diagram indicates the position of the hub square around which the piece rotates.
 * <p>
 * <code>
 * <p>
 * NORTH
 * ◽️🔄🐯
 * 🐯🐯
 * <p>
 * WEST
 * 🐯
 * 🐯🔄
 * ◽️🐯
 * <p>
 * SOUTH
 * ◽️🔄🐯
 * 🐯🐯
 * <p>
 * EAST
 * 🐯
 * 🐯🔄
 * ◽️🐯
 * <p>
 * </code>
 *
 * @author @JoeSondow
 */
public class ShapeEss extends Shape {

    /**
     * Creates a new <code>ShapeEss</code> object, also called a piece.
     *
     * @param config the Game object that owns the new piece, and Direction the piece faces
     */
    ShapeEss(ShapeConfig config) {
        super(ShapeType.ESS, config);
    }

    @Override
    public ShapeType getShapeType() {
        return ShapeType.ESS;
    }

    /**
     * Updates the positions of all the tiles in this piece, based on: <br>
     * - the configuration of <code>ShapeEss</code>, <br>
     * - the piece's current rotation (NORTH, EAST, SOUTH, or WEST), <br>
     * - the column index and row index of the hub square for this piece
     *
     * @throws BadCoordinatesException If updating the tile positions causes an illegal situation,
     *                                 such as a tile intersecting another or being off the game
     *                                 grid. This generally results in a "Game Over" condition.
     */
    @Override
    public void updateTiles() throws BadCoordinatesException {
        int col = columnIndex, row = rowIndex;
        if (direction == Direction.NORTH || direction == Direction.SOUTH) {
            // Configuration appears as follows (@ is hub square):
            //
            //  @+
            // ++
            setTilePosition(1, col, row);
            setTilePosition(2, col + 1, row);
            setTilePosition(3, col, row + 1);
            setTilePosition(4, col - 1, row + 1);
        } else if (direction == Direction.EAST || direction == Direction.WEST) {
            // Configuration appears as follows (@ is hub square):
            //
            // +
            // @+
            //  +
            setTilePosition(1, col, row);
            setTilePosition(2, col, row - 1);
            setTilePosition(3, col + 1, row);
            setTilePosition(4, col + 1, row + 1);
        }
    }
}
