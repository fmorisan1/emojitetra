package sondow.tetra.shape;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Move;

import static sondow.tetra.shape.Shape.COLUMN_START;
import static sondow.tetra.shape.Shape.ROW_START;

public enum ShapeType {
    ZEE(ShapeZee::new, "Z", "⠞"),
    LINE(ShapeLine::new, "I", "⡇"),
    TEE(ShapeTee::new, "T", "⠗"),
    JAY(ShapeJay::new, "J", "⠼"),
    SQUARE(ShapeSquare::new, "O", "⠶", Rotatability.DISABLED),
    ELL(ShapeEll::new, "L", "⠧"),
    ESS(ShapeEss::new, "S", "⠳");

    private Function<ShapeConfig, Shape> create;

    /**
     * Used only for database storage.
     */
    private String letterRepresentation;

    /**
     * Used only for display of "Next" shape to players.
     */
    private String unicodeRepresentation;

    /**
     * Some shapes like squares are not changed by rotating so the Rotate option should be omitted.
     */
    private Rotatability rotatability = Rotatability.ENABLED;

    ShapeType(Function<ShapeConfig, Shape> create, String letter, String unicodeRepresentation) {
        this.create = create;
        this.letterRepresentation = letter;
        this.unicodeRepresentation = unicodeRepresentation;
    }

    ShapeType(Function<ShapeConfig, Shape> create, String letter, String unicodeRepresentation,
              Rotatability rotatability) {
        this(create, letter, unicodeRepresentation);
        this.rotatability = rotatability;
    }

    private static Map<String, ShapeType> REVERSE_LOOKUP = new HashMap<>();

    static {
        ShapeType[] shapeTypes = values();
        for (ShapeType shapeType : shapeTypes) {
            REVERSE_LOOKUP.put(shapeType.letterRepresentation, shapeType);
        }
    }

    public static ShapeType pickAny(Random random) {
        ShapeType[] values = values();
        return values[random.nextInt(values.length)];
    }

    public static Shape createAny(Random random, Game context) {
        return createShapeAtSpawnPoint(pickAny(random), context, Direction.NORTH);
    }

    public static Shape createShape(ShapeType shapeType, Game context, Direction direction, int
            rowIndex, int colIndex, Move previousMove) {
        ShapeConfig config = new ShapeConfig(context, direction, rowIndex, colIndex,
                previousMove);
        Function<ShapeConfig, Shape> createFunc = shapeType.create;
        return createFunc.apply(config);
    }

    public static Shape createShapeAtSpawnPoint(ShapeType type, Game context, Direction direction) {
        return createShape(type, context, direction, ROW_START, COLUMN_START, null);
    }

    public static ShapeType fromLetter(String letter) throws NoSuchShapeException {
        if (isValidLetter(letter)) {
            return REVERSE_LOOKUP.get(letter);
        } else {
            throw new NoSuchShapeException(letter);
        }
    }

    public static boolean isValidLetter(String letter) {
        return REVERSE_LOOKUP.containsKey(letter);
    }

    public String getLetterRepresentation() {
        return letterRepresentation;
    }

    /**
     * Gets the character that most closely resembles this shape, for preview purposes and testing.
     *
     * @return the character than most closely resembles this shape
     */
    public String getUnicodeRepresentation() {
        return unicodeRepresentation;
    }

    /**
     * Some shapes like squares are not changed by rotating so the Rotate option should be omitted.
     */
    public Rotatability getRotatability() {
        return rotatability;
    }
}
