package sondow.tetra.shape;

import java.util.Arrays;
import java.util.Collections;
import sondow.tetra.game.BadCoordinatesException;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Move;
import sondow.tetra.game.Tile;

/**
 * Also called a piece or tetramino, a Tetris <code>Shape</code> is a unit consisting of one or more
 * <code>Tile</code>s in a contiguous set of four. One piece descends at a time, and can be
 * rotated and shifted left and right by the player during descent.
 * <p>
 * This class is abstract because a <code>Shape</code> must be instantiated with a chosen
 * configuration, such as a T-shape or a 2x2 square. All shapes behave essentially the same way,
 * according to the methods <code>left()</code>, <code>right()</code>, <code>down()</code>,
 * <code>stopTiles()</code>, <code>rotate()</code> in this abstract <code>Shape</code> class. The
 * difference between shape subclasses is what configuration the tiles in the shape must take
 * whenever the tiles get updated with the <code>updateTiles()</code> method, which must be
 * implemented by a subclass of <code>Shape</code>.
 *
 * @author @JoeSondow
 */
public abstract class Shape {

    static final int ROW_START = 0;
    static final int COLUMN_START = 3;

    /**
     * Number one of the four <code>Tile</code> objects in this piece
     */
    private Tile tile1;
    /**
     * Number two of the four <code>Tile</code> objects in this piece
     */
    private Tile tile2;
    /**
     * Number three of the four <code>Tile</code> objects in this piece
     */
    private Tile tile3;
    /**
     * Number four of the four <code>Tile</code> objects in this piece
     */
    private Tile tile4;

    /**
     * The current column index of the hub square of this piece
     */
    int columnIndex;

    /**
     * The current row index of the hub square of this piece
     */
    int rowIndex;

    protected Direction direction = Direction.NORTH;

    private Move previousMove;

    /**
     * Flag to indicate whether this piece is currently falling, which generally means that the
     * game is not yet over
     */
    private boolean falling = true;

    /**
     * The game panel in which this piece is displayed and from which this piece is being
     * manipulated
     */
    public final Game context;

    /**
     * Creates a new <code>Shape</code> object, also called a piece. Since <code>Shape</code> is
     * abstract, this constructor only gets called by subclasses of <code>Shape</code>. This
     * constructor then calls <code>updateTiles()</code> which is different for each shape subclass.
     *
     * @param shapeType the type of shape
     * @param config    the Game object that owns the new piece, and the Direction the piece faces
     */
    protected Shape(ShapeType shapeType, ShapeConfig config) {
        direction = config.getDirection();
        context = config.getContext();
        rowIndex = config.getRowIndex();
        columnIndex = config.getColumnIndex();
        previousMove = config.getPreviousMove();

        tile1 = new Tile(shapeType, context); // hub
        tile2 = new Tile(shapeType, context);
        tile3 = new Tile(shapeType, context);
        tile4 = new Tile(shapeType, context);
        updateTiles();
    }

    /**
     * Moves the currently active piece left.
     */
    public void left() throws BadCoordinatesException {
        columnIndex--; // Move the shape left
        updateTiles(); // Align all the tiles to match the hub tile
    }

    /**
     * Moves the currently active piece right.
     */
    public void right() throws BadCoordinatesException {
        columnIndex++;
        updateTiles(); // Align all the tiles to match the hub tile
    }

    /**
     * Moves the currently active piece down.
     */
    public void down() {
        rowIndex++;
        updateTiles(); // Align all the tiles to match the hub tile
    }

    /**
     * Stops the piece and moves its tiles into the set of non-moving tiles on the game grid.
     */
    public void stop() {

        // Only try to stop the tiles if they are still falling
        // This prevents a multi-threading bug where 2 threads can try to stop
        // the same piece.

        context.addTile(tile1, tile1.getRow(), tile1.getCol());
        context.addTile(tile2, tile2.getRow(), tile2.getCol());
        context.addTile(tile3, tile3.getRow(), tile3.getCol());
        context.addTile(tile4, tile4.getRow(), tile4.getCol());

        falling = false;
    }

    /**
     * Rotates the currently active piece counter-clockwise.
     */
    public void rotate() throws BadCoordinatesException {
        switch (direction) {
            case NORTH:
                direction = Direction.WEST;
                break;
            case WEST:
                direction = Direction.SOUTH;
                break;
            case SOUTH:
                direction = Direction.EAST;
                break;
            case EAST:
                direction = Direction.NORTH;
                break;
        }
        updateTiles(); // Align all tiles to match hub tile and rotation
    }

    /**
     * Rearranges the tiles in this piece to match the piece's new position and rotation. If any
     * of the piece's tiles falls in an illegal space, a BadCoordinatesException gets thrown.
     *
     * @throws BadCoordinatesException if any of the piece's tiles falls in an illegal space
     */
    public abstract void updateTiles() throws BadCoordinatesException;

    /**
     * Sets a new position for one of the 4 tiles in this piece.
     *
     * @param tileNum the number of the tile, out of the 4 tiles in this piece (1, 2, 3, or 4)
     * @param col     the column index of the new position
     * @param row     the row index of the new position
     * @throws IllegalArgumentException if tileNum is less than 1 or greater than 0
     * @throws BadCoordinatesException  if the specified position is illegal
     */
    void setTilePosition(int tileNum, int col, int row)
            throws IllegalArgumentException, BadCoordinatesException {
        Tile theTile = chooseTile(tileNum);
        theTile.setPos(col, row);
    }

    /**
     * Retrieves the enum value for the type of shape
     *
     * @return the shape type
     */
    public abstract ShapeType getShapeType();

    /**
     * Retrieves the current column index of the specified tile in this piece.
     *
     * @param tileNum the number of the tile, out of the 4 tiles in this piece (1, 2, 3, or 4)
     * @return the column index of the specified tile
     * @throws IllegalArgumentException if tileNum is less than 1 or greater than 0
     */
    public int getTileCol(int tileNum) throws IllegalArgumentException {
        Tile theTile = chooseTile(tileNum);
        return theTile.getCol();
    }

    /**
     * Retrieves the current row index of the specified tile in this piece.
     *
     * @param tileNum the number of the tile, out of the 4 tiles in this piece (1, 2, 3, or 4)
     * @return the row index of the specified tile
     * @throws IllegalArgumentException if tileNum is less than 1 or greater than 0
     */
    public int getTileRow(int tileNum) throws IllegalArgumentException {
        Tile theTile = chooseTile(tileNum);
        return theTile.getRow();
    }

    // Select the appropriate Tile object from this Shape object, by number.
    // This may be refactored later to use an array or List of Tiles, but with
    // only 4 tiles for each piece in this version, this is fine for now.
    private Tile chooseTile(int tileNum) throws IllegalArgumentException {
        Tile theTile;
        switch (tileNum) {
            case 1:
                theTile = tile1;
                break;
            case 2:
                theTile = tile2;
                break;
            case 3:
                theTile = tile3;
                break;
            case 4:
                theTile = tile4;
                break;
            default:
                throw new IllegalArgumentException("Invalid number for tile. Only 1-4 are valid");
        }
        return theTile;
    }

//    /**
//     * Checks to see if this piece has failed to find a legal position, implying game over.
//     *
//     * @return <code>true</code> if the piece has failed, <code>false</code> otherwise
//     */
//    public boolean hasFailed() {
//        return failed;
//    }

    /**
     * Checks to see if this piece is still falling.
     *
     * @return <code>true</code> if the piece is still falling, <code>false</code> otherwise
     */
    public boolean isFalling() {
        return falling;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public Shape copy() {

        ShapeType shapeType = getShapeType();
        int rowIndex = getRowIndex();
        int colIndex = getColumnIndex();
        Direction direction = getDirection();
        Move prevMove = getPreviousMove();
        return ShapeType.createShape(shapeType, context, direction, rowIndex, colIndex, prevMove);
    }

    public int getBottomTileRowIndex() {
        int tr1 = tile1.getRow();
        int tr2 = tile2.getRow();
        int tr3 = tile3.getRow();
        int tr4 = tile4.getRow();
        return Collections.max(Arrays.asList(tr1, tr2, tr3, tr4));
    }

    public Move getPreviousMove() {
        return previousMove;
    }

    public void setPreviousMove(Move previousMove) {
        this.previousMove = previousMove;
    }

    @Override
    public String toString() {
        return "col: " + columnIndex + ", row: " + rowIndex + ", shapetype:" + getShapeType() +
                ", direction: " + getDirection() +
                ", bottom tile row index " + getBottomTileRowIndex();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Shape shape = (Shape) o;

        if (columnIndex != shape.columnIndex) {
            return false;
        }
        if (rowIndex != shape.rowIndex) {
            return false;
        }
        if (falling != shape.falling) {
            return false;
        }
        if (tile1 != null ? !tile1.equals(shape.tile1) : shape.tile1 != null) {
            return false;
        }
        if (tile2 != null ? !tile2.equals(shape.tile2) : shape.tile2 != null) {
            return false;
        }
        if (tile3 != null ? !tile3.equals(shape.tile3) : shape.tile3 != null) {
            return false;
        }
        if (tile4 != null ? !tile4.equals(shape.tile4) : shape.tile4 != null) {
            return false;
        }
        return direction == shape.direction;
    }

    @Override
    public int hashCode() {
        int result = tile1 != null ? tile1.hashCode() : 0;
        result = 31 * result + (tile2 != null ? tile2.hashCode() : 0);
        result = 31 * result + (tile3 != null ? tile3.hashCode() : 0);
        result = 31 * result + (tile4 != null ? tile4.hashCode() : 0);
        result = 31 * result + columnIndex;
        result = 31 * result + rowIndex;
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        result = 31 * result + (falling ? 1 : 0);
        return result;
    }

}
