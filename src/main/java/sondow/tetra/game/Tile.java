package sondow.tetra.game;

import sondow.tetra.shape.ShapeType;

/**
 * A single game tile (square) of a tetramino represented by a certain emoji.
 *
 * @author @JoeSondow
 */
public class Tile {

    /**
     * The type of shape this tile originally belonged to, which dictates the tile's appearance.
     */
    private final ShapeType shapeType;

    /**
     * The x-coordinate (column) of the tile's current position
     */
    private int col = -1;

    /**
     * The y-coordinate (row) of the tile's current position
     */
    private int row = -1;

    /**
     * The Tetris game object where this tile is used. The tile must have access to the game data
     * in order to keep itself from entering an illegal position
     */
    private Game context;

    /**
     * @return the shape type
     */
    public ShapeType getShapeType() {
        return shapeType;
    }

    /**
     * Returns the tile's current column index. The leftmost column should be zero. If this
     * returns -1, the tile is not on the game grid, probably because it would not fit (game over).
     *
     * @return the column index of the tile
     */
    public int getCol() {
        return col;
    }

    /**
     * Sets the tile's column index. Making it greater moves the tile right. Making it lesser
     * moves the tile left. The setPos method calls this method and handles any errors.
     *
     * @param col the new column index
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Returns the current row index of the tile. The top row should be zero. If this returns -1,
     * the tile is not on the game grid, probably because it would not fit (game over).
     *
     * @return the row index of the tile
     */
    public int getRow() {
        return row;
    }

    /**
     * Sets the tile's row index. Making it greater moves the tile down. Making it lesser moves
     * the tile up. The setPos method calls this method and handles any errors.
     *
     * @param row the new row index
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Sets the column and row index values of the tile, thereby moving the tile to a new legal
     * position on the game grid. The new position is legal if and only if it is a blank square on
     * the grid. If the new position is illegal, this method throws a BadCoordinatesException.
     *
     * @param col the new column index
     * @param row the new row index
     * @throws BadCoordinatesException if new position is illegal
     */
    public void setPos(int col, int row) throws BadCoordinatesException {
        // if off the grid or if there is already a non-moving tile at this position
        if (col < 0 || row < 0 || col >= context.getColumnCount() || row >= context.getRowCount() ||
                context.hasTileAt(row, col)) {
            throw new BadCoordinatesException(col, row);
        }
        // Position is legal, so change the tile's position values
        setCol(col);
        setRow(row);
    }

    /**
     * Creates a new <code>Tile</code> object with a specified <code>Color</code> and game grid
     * context in which to exist.
     *
     * @param shapeType this tells the tile what type of appearance to have
     * @param owner     the game grid context in which this tile will exist
     */
    public Tile(ShapeType shapeType, Game owner) {
        this.shapeType = shapeType;
        context = owner;
    }

    /**
     * Returns a string description of the tile.
     *
     * @return a string description of the tile
     */
    @Override
    public String toString() {
        return "Tile[" + col + ", " + row + ", " + shapeType + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tile tile = (Tile) o;

        if (col != tile.col) {
            return false;
        }
        if (row != tile.row) {
            return false;
        }
        return shapeType == tile.shapeType;
    }

    @Override
    public int hashCode() {
        int result = shapeType != null ? shapeType.hashCode() : 0;
        result = 31 * result + col;
        result = 31 * result + row;
        return result;
    }
}
