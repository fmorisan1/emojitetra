package sondow.tetra.io;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import twitter4j.CardLight;
import twitter4j.CardsTwitterImpl;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.StatusUpdateWithCard;
import twitter4j.StatusWithCard;
import twitter4j.TwitterException;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * See JavaScript example at https://gist.github.com/fourtonfish/816c5272c3480c7d0e102b393f60bd49.
 * <p>
 * See Python example at https://gist.github.com/fourtonfish/5ac885e5e13e6ca33dca9f8c2ef1c46e
 * <p>
 * More reference to understand the examples above.
 * <p>
 * Endpoints: https://github.com/ttezel/twit/blob/master/lib/endpoints.js
 * <p>
 * Endpoints are also available in twitter4j at twitter4j.conf.ConfigurationBase but
 * caps.twitter.com is missing
 * because it isn't a publicly documented part of Twitter's API.
 * <p>
 * JavaScript promise library Q: https://github.com/kriskowal/q
 *
 * @author @JoeSondow
 */
public class TwitterPollMaker {

    private CardsTwitterImpl twitter;

    public TwitterPollMaker(Configuration twitterConfig) {
        Authorization auth = AuthorizationFactory.getInstance(twitterConfig);
        twitter = new CardsTwitterImpl(twitterConfig, auth);
    }

    public Status tweet(String text, Long inReplyToStatusId) {
        StatusUpdate update = new StatusUpdate(text);
        if (inReplyToStatusId != null && inReplyToStatusId > 0) {
            update.setInReplyToStatusId(inReplyToStatusId);
        }
        try {
            return twitter.updateStatus(update);
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
    }

    public Status postPoll(int durationMinutes, String statusText, List<String> choices,
                           Long inReplyToStatusId) {

        int choicesCount = choices.size();
        Map<String, String> params = new LinkedHashMap<>();
        params.put("twitter:api:api:endpoint", "1");
        params.put("twitter:card", "poll" + choices.size() + "choice_text_only");
        params.put("twitter:long:duration_minutes", Integer.toString(durationMinutes));
        ensureTwoToFourChoices(choicesCount);

        for (int i = 0; i < choices.size(); i++) {
            String entry = choices.get(i);
            params.put("twitter:string:choice" + (i + 1) + "_label", entry);
        }
        try {
            CardLight card = twitter.createCard(params);
            StatusUpdateWithCard update = new StatusUpdateWithCard(statusText, card.getCardUri());
            if (inReplyToStatusId != null && inReplyToStatusId > 0) {
                update.setInReplyToStatusId(inReplyToStatusId);
            }
            return twitter.updateStatus(update);
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
    }

    private void ensureTwoToFourChoices(int choicesCount) {
        if (choicesCount < 2) {
            throw new RuntimeException("Must have at least two poll choices");
        }
        if (choicesCount > 4) {
            throw new RuntimeException("Too many poll choices (max 4)");
        }
    }

    public StatusWithCard readPreviousTweet(Long tweetId) {
        StatusWithCard tweet;
        if (tweetId == null) {
            tweet = null;
        } else {
            try {
                tweet = twitter.showStatusWithCard(tweetId);
            } catch (TwitterException e) {
                throw new RuntimeException(e);
            }
        }
        return tweet;
    }

    public static void main(String[] args) throws TwitterException {
        ConfigurationBuilder cardsConfigBuilder = new ConfigurationBuilder();
        cardsConfigBuilder.setTrimUserEnabled(true);
        Configuration cardsConfig = cardsConfigBuilder.build();
        Authorization auth = AuthorizationFactory.getInstance(cardsConfig);
        CardsTwitterImpl twitter = new CardsTwitterImpl(cardsConfig, auth);

        // Characters I was going to use but decided not to because they're either only available as
        // two-character symbols, or they're available as both one- and two-character symbols but
        // the one character version does not render consistently in different contexts, and I was
        // able to find a good substitute:
        // Baseball, Red Heart, Small White Square
        // The best empty space character I've found so far is Medium White Square ◽ but be
        // careful not to use the version that renders the same but takes up four characters on
        // Twitter.
        String message = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽🐻🐻◽◽◽\n" +
                "◽◽◽🐻🐻◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽🐯◽\n" +
                "◽◽🐯🐯🐼🐯🐯\n" +
                "◽🐯🐯🐼🐼🐼🐯\n";
        twitter.updateStatus(message);
        // "️▫️🥅🏓🏀🏉🎾⚾🏈🎱"
        // "️▫️🥅🏓🏀🏉🎾⚾️🏈🎱"
        // "🐚🦑🦀🐡🐠🐟🐙🦈\n" 1 character each
        // "📖📕📙📒📗📘📔📚" 1 character each
        // "▫️" 2 characters
    }

}
